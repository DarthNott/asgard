import React from "react";
import gql from "graphql-tag";
import { Query, graphql, Mutation } from "react-apollo";
import Link from "next/link";
import TimeAgo from "react-timeago";
import withData from "../lib/withData";
// Queries & Mutations
import GET_ANSWERS from "../queries/answers";
// Components & Forms
import Answer from "../components/Answer-new";


class AnswersList extends React.Component {
  constructor(props) {
		super(props)
		this.state = {
			limit: 5,
			skip: 0,
			showButton: true,
			loading: true
		}
  }
  
  render() {
    return (
      <Query query={GET_ANSWERS} fetchPolicy="cache-and-network" variables={{ questionId: this.props.questionId, limit: this.state.limit, skip: this.state.skip }}>
        {({ loading, error, data: { answers }, fetchMore }) => {
          {/* if (loading && this.state.loading) return "Loading..."; */}
          if (error) return `Error! ${error.message}`;
          if (!answers) return "Not found";
          
          return (
            <div>
              <ul>
                {answers.map((answer, index) => (
                  <Answer key={index} {...answer}/>
                ))}
              </ul>

              {loading && answers.length >= this.state.limit ? <div>Loading</div> : ""}
              
              {(answers.length >= this.state.limit) && this.state.showButton 
              ? (
                <button onClick={() => {
                  this.setState({ loading: false })
                  fetchMore({
                    variables: { skip: answers.length },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if(fetchMoreResult.answers.length < this.state.limit) this.setState({showButton: false});
                      if (!fetchMoreResult) return prev;
                      return Object.assign({}, prev, {
                        answers: [...prev.answers, ...fetchMoreResult.answers]
                      });
                    }
                  })	
                }}>Show {this.props.question.answerCounter - answers.length} more</button>
              ) : (
                <div></div>
              )}

            </div>
          )
        }}
      </Query>
    )
  }
}

export default withData(AnswersList);