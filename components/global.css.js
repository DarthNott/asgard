import { injectGlobal } from 'styled-components'

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
  * {
    box-sizing: border-box;
    font-family: 'Open Sans', sans-serif;
    margin: 0;
    border: 0;
  }

  h1 {
    font-size: 40px;
  }

  input, button {
    border: 1px solid #d2d2d2;
  }
`