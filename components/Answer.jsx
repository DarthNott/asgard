import React from "react";
import gql from "graphql-tag";
import { Query, graphql, Mutation } from "react-apollo";
import Link from "next/link";
import TimeAgo from "react-timeago";
import withData from "../lib/withData";
// Queries & Mutations
import LIKE_ANSWER from "../mutations/likeAnswer";


class Answer extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			liked: this.props.liked,
			like_Counter: this.props.likeCounter
		}
	}

	render() {
		// id für anchor -> link #answerId = zum anchor springen
		return (
			<li id={this.props._id}> 
        <span><b>{this.props.content}</b> by <Link href={`/u/${this.props.author.username}`}>{this.props.author.username}</Link></span>
        <br/>
        <TimeAgo date={this.props.createdAt} />
        <br/>
        <span>Likes: {this.state.like_Counter}</span>
        <br/>
        <span>Liked: {this.state.liked.toString()}</span>
        <br/>
        <Mutation mutation={LIKE_ANSWER}>
          {likeAnswer => (
            <button onClick={() => {
              likeAnswer({ variables: { answerId: this.props._id } })
              this.setState({
                liked: !this.state.liked,
                like_Counter: this.state.liked ? (this.state.like_Counter - 1) : (this.state.like_Counter + 1)
              })
            }}>Like</button>
          )}
        </Mutation>
			</li>
		)
	}
}

export default withData(Answer);