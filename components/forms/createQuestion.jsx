import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

class CreateQuestion extends React.Component {
  state = {
    title: null,
    content: null,
    error: null
  }

  onFormSubmit = e => {
    e.preventDefault()
    let { title, content } = this.state

    if (typeof title === 'string' && typeof content === 'string') {
      title = title.trim()
      content = content.trim()

      if (title.length > 0 && content.length > 0) {
        this.props
          .mutate({
            variables: {
              title,
              content
            }
          })
          .then(() => {
            this.setState({ error: null })
            window.location = '/questions'
          })
          .catch(({ graphQLErrors: err }) =>
            this.setState({ error: err.message })
          )
      } else {
        this.setState({ error: 'Title and content shouldn’t be empty'})
      }
    }
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <div>
          <span className='error'>{this.state.error}</span>
          <label>Title</label>
          <input
            type="title"
            onInput={e => this.setState({ title: e.target.value })}
          />
        </div>
        <div>
          <label>Content</label>
          <input
            type="text"
            onInput={e => this.setState({ content: e.target.value })}
          />
        </div>
        <div>
          <button>Create Question</button>
        </div>
      </form>
    )
  }
}

const mutator = gql`
	mutation createQuestion($title: String!, $content: String!) {
		createQuestion(title: $title, content: $content) {
			title
      content
		}
	}
`
export default graphql(mutator)(CreateQuestion)