import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

class CreateAnswer extends React.Component {
  state = {
    questionId: this.props.questionId,
    content: null,
    error: null
  }

  onFormSubmit = e => {
    e.preventDefault()
    let { questionId, content } = this.state

    if (typeof content === 'string') {
      content = content.trim()

      if (content.length > 0) {
        this.props
          .mutate({
            variables: {
              questionId,
              content
            }
          })
          .then(() => {
            this.setState({ error: null })
            window.location = '/question/' + questionId
          })
          .catch(({ graphQLErrors: err }) =>
            this.setState({ error: err.message })
          )
      } else {
        this.setState({ error: 'Content shouldn’t be empty'})
      }
    }
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <div>
          <label>Content</label>
          <span className='error'>{this.state.error}</span>
          <input
            type="text"
            onInput={e => this.setState({ content: e.target.value })}
          />
        </div>
        <div>
          <button>Create Answer</button>
        </div>
      </form>
    )
  }
}

const mutator = gql`
	mutation createAnswer($questionId: String!, $content: String!) {
		createAnswer(questionId: $questionId, content: $content) {
      content
		}
	}
`
export default graphql(mutator)(CreateAnswer)