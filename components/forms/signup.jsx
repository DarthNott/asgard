import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

class signup extends React.Component {
	state = {
		email: null,
		username: null,
		password: null,
		nativeLanguage: null,
		error: null
	}

	onFormSubmit = e => {
		e.preventDefault()
		let { email, username, password, nativeLanguage } = this.state

		// Check non null email && password
		if (typeof email === 'string' && typeof password === 'string') {
			// trim fields
			email = email.trim()
			username = username.trim()
			password = password.trim()

			// Check for email && password length
			if (email.length > 0 && password.length > 0) {
				this.props
					.mutate({
						variables: {
							email,
							username,
							password,
							nativeLanguage
						}
					})
					.then(() => {
						this.setState({ error: null })
						window.location = '/'
					})
					.catch(({ graphQLErrors: err }) =>
						this.setState({ error: err[0].message })
					)
			} else {
				this.setState({ error: "Email & Password Field shouldn't be empty" })
			}
		} else {
			this.setState({ error: 'Email & Password Field Required!' })
		}
	}

	render() {
		return (
			<form onSubmit={this.onFormSubmit}>
				<div>
					<span className="error">{this.state.error}</span>
					<label> Email Address </label>
					<input
						type="email"
						onInput={e => this.setState({ email: e.target.value })}
						placeholder="example@mail.com"
					/>
				</div>
				<div>
					<label> Username </label>
					<input
						type="text"
						onInput={e => this.setState({ username: e.target.value })}
						placeholder="username"
					/>
				</div>
				<div>
					<label> Password </label>
					<input
						type="password"
						onInput={e => this.setState({ password: e.target.value })}
						placeholder="******"
					/>
				</div>
				<div>
					<label> Native language</label>
					<select name="nativeLanguage" onChange={e => this.setState({ nativeLanguage: e.target.value })}>
						{/* <option value="" selected disabled hidden>Choose</option> */}
						<option value="English" selected>English</option>
						<option value="German">German</option>
						<option value="Spanish">Spanish</option>
					</select>
				</div>
				<div>
					<button> Sign up </button>
				</div>
				<style jsx>
					{`
						* {
							box-sizing: border-box;
							margin: 0;
						}

						h1 {
							margin: 2rem 0;
						}

						label {
							display: block;
						}

						form > div {
							margin-top: 1rem;
						}

						input,
						button {
							padding: 0.5rem;
						}

						button {
							width: 12rem;
							border: none;
							cursor: pointer;
						}

						.error {
							color: red;
							display: block;
							margin: 1rem 0;
						}
					`}
				</style>
			</form>
		)
	}
}

const mutator = gql`
	mutation createUser($email: String!, $username: String!, $password: String!, $nativeLanguage: String!) {
		createUser(email: $email, username: $username, password: $password, nativeLanguage: $nativeLanguage) {
			email
		}
	}
`
export default graphql(mutator)(signup)
