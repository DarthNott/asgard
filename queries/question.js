import gql from "graphql-tag";

const GET_QUESTION = gql`
	query question($questionId: String!) { 
		question(id: $questionId) {
			_id
      title
      content
      author {
        username
				xp
      }
			answerCounter
      authorId
      createdAt
		}
	}
`

export default GET_QUESTION;