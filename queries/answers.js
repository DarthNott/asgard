import gql from "graphql-tag";

const GET_ANSWERS = gql`
	query answers($questionId: String!, $limit: Int!, $skip: Int) {
		answers(limit: $limit, skip: $skip, questionId: $questionId) {
			_id
			content
			createdAt
			likeCounter
			liked
      author {
        username
      }
		}
	}
`

export default GET_ANSWERS;