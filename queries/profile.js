import gql from "graphql-tag";

const GET_USER = gql`
	query profile($username: String!) { 
		profile(username: $username) {
			_id
			username
			name
			bio
			nativeLanguage
			learningLanguage
			xp
		}
		questions(limit: 0, author: $username) {
			_id
			title
			content
			createdAt
			answerCounter
		}
	}
`

export default GET_USER;