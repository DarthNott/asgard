import gql from "graphql-tag";

const GET_QUESTIONS = gql`
  query questions($limit: Int!) {
    questions(limit: $limit, skip: 0) {
      _id
      title
      content
      author {
				username
				xp
			}
			authorId
			answerCounter
      answers {
				content
			}
      createdAt
    }
  }
`

export default GET_QUESTIONS;