import gql from "graphql-tag";

const LIKE_ANSWER = gql`
	mutation likeAnswer($answerId: String!) {
		likeAnswer(answerId: $answerId) {
			_id
		}
	}
`

export default LIKE_ANSWER;