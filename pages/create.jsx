import Router from "next/router";
import gql from 'graphql-tag'

import checkLoggedIn from "../lib/checkLoggedIn";
import withData from "../lib/withData";

import CreateQuestion from "../components/forms/createQuestion"

class Create extends React.Component {
	static async getInitialProps(context, apolloClient) {
		const { loggedInUser } = await checkLoggedIn(context, apolloClient);
		// Check ob der User eingeloggt ist und ob er SSR oder CSR
		if (!loggedInUser.profile) {
			if (context.res) {
				context.res.writeHead(302, {
					Location: "/login"
				});
				context.res.end();
			} else {
				Router.push("/login");
			}
		}

		/* FÜR ANDERE SACHEN WO MAN INDIVIDUELLE WERTE BRAUCHT */
    // const { user } = await apolloClient
		// 	.query({ query: gql`
		// 			query profile {
		// 				profile {
		// 					email
		// 					username
		// 					bio
		// 					nativeLanguage
		// 					xp
		// 				}
		// 			}
		// 		`
		// 	})
		// .then(({ data }) => {
		// 	return { user: data.profile };
		// })
		// .catch(e => {
		// 	return { user: false };
		// });
    //   if (!user) {
    //     if (context.res) {
    //       context.res.writeHead(302, {
    //         Location: "/login"
    //       });
    //       return context.res.end();
    //     } else {
    //       return Router.push("/login");
    //     }
    //   }
    //   return {
		// 	  user: user 
		// 	};
			/* FÜR ANDERE SACHEN WO MAN INDIVIDUELLE WERTE BRAUCHT */

		return { user: loggedInUser.profile };
	}
  // FÜR CREATE BRAUCHT MAN KEINE GROßE QUERY -> die obere im Kommentar reicht!
	render() {
    const { user } = this.props;
		return (
			<div>
				<h1>Create Question</h1>
				<CreateQuestion/>
			</div>
		);
	}
}

export default withData(Create);