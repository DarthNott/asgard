import React from 'react'
import Router from 'next/router'
import gql from 'graphql-tag' // NEU

// import checkLoggedIn from '../lib/checkLoggedIn'
import withData from '../lib/withData'

class Profile extends React.Component {
	static async getInitialProps(context, apolloClient) {
		// const { loggedInUser } = await checkLoggedIn(context, apolloClient)

		// return { user: loggedInUser.profile }

		// NEU ↓
		const { user } = await apolloClient
			.query({ query: gql`
					query profile {
						profile {
							email
							username
							bio
							nativeLanguage
							xp
						}
					}
				`
			})
		.then(({ data }) => {
			return { user: data.profile };
		})
		.catch(e => {
			return { user: false };
		});
		return {
			user: user
		};
		// NEU ↑
	}

	render() {
		const { user } = this.props

		if (user) {
			return (
				<div>
					<p>My Profile</p>
					<p>Username: {user.username}</p>
					<p>Email: {user.email}</p>
					<p>Bio: {user.bio}</p>
					<p>Native language: {user.nativeLanguage}</p>
					<p>XP: {user.xp}</p>
				</div>
			)
		}

		return <div>Not found!</div>
	}
}

export default withData(Profile)
