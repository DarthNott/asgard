import React from "react";
import gql from "graphql-tag";
import { Query, graphql, Mutation } from "react-apollo"; // NEU: graphql
import Link from "next/link";
import TimeAgo from "react-timeago";
// import checkLoggedIn from "../lib/checkLoggedIn";
import withData from "../lib/withData";
// Queries & Mutations
import GET_QUESTION from "../queries/question";
import GET_ANSWERS from "../queries/answers";
import LIKE_ANSWER from "../mutations/likeAnswer";
// Components & Forms
import CreateAnswer from "../components/forms/createAnswer";
import Answer from "../components/Answer";
import AnswersList from "../components/AnswersList";


class Question extends React.Component {
	static async getInitialProps(context, apolloClient) {
		return { query: context.query.questionId, queryAnswer: context.query.answerId };
	}
	constructor(props) {
		super(props);
		this.state = {
			questionId: props.query,
			answerId: props.queryAnswer // erstmal drin zum scrollen zu einer Frage - funktioniert noch nicht
		};
	}
	

	render() {
		return (
			<div>
				<Query query={GET_QUESTION} variables={{ questionId: this.state.questionId }}>
					{({ loading, error, data: { question } }) => { // NEU: fetchMore
						if (loading) return "Loading...";
						if (error) return `Error! ${error.message}`;
						if (!question) return "Not found";

						return (
							<div>
								<h2>Question</h2>
								<p>Title: {question.title}</p>
                <p>Content: {question.content}</p>
								<p>Asked by: {question.author.username} ({question.author.xp} XP)</p>
                <TimeAgo date={question.createdAt} />
								<h2>Answers</h2>
								<CreateAnswer questionId={this.state.questionId}/>
								<AnswersList question={question} questionId={this.state.questionId}/>
							</div>
						)
					}}
				</Query>
			</div>
		);
	}
}

export default withData(Question);