import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Link from "next/link";
import TimeAgo from "react-timeago";
// import checkLoggedIn from "../lib/checkLoggedIn";
import withData from "../lib/withData";
// Queries
import GET_USER from "../queries/profile";

// const GET_USER = gql`
// 	query profile($username: String!) { 
// 		profile(username: $username) {
// 			_id
// 			username
// 			name
// 			bio
// 			nativeLanguage
// 			learningLanguage
// 			xp
// 		}
// 		questions(limit: 0, author: $username) {
// 			_id
// 			title
// 			content
// 			createdAt
// 			answerCounter
// 		}
// 	}
// `;

class UserProfile extends React.Component {
	static async getInitialProps(context, apolloClient) {
		return { query: context.query.username };
	}
	constructor(props) {
		super(props);
		this.state = {
			username: props.query
		};
	}

	render() {
		return (
			<div>
				<Query query={GET_USER} variables={{ username: this.state.username }}>
					{({ loading, error, data }) => {
						if (loading) return "Loading...";
						if (error) return `Error! ${error.message}`;
						if (!data.profile) return "Not found";

						return (
							<div>
								<h2>Profile</h2>
								<p>Username: {data.profile.username}</p>
								<p>Name: {data.profile.name}</p>
								<p>Self-introduction: {data.profile.bio}</p>
								<p>Native language: {data.profile.nativeLanguage}</p>
								<div>Languages of interest: {data.profile.learningLanguage.map((lang, index) => (
									<p key={index}>{lang}</p>
								))}</div>
                <p>XP: {data.profile.xp}</p>
								<ul>
									{data.questions.map((question, index) => (
										<li key={index}>
											<p>
											<Link href={`/question/${question._id}`}><b>{question.title}</b></Link>
												<br />
												<span>{question.content}</span>
												<br />
												<TimeAgo date={question.createdAt} />
												<br/>
												<span>Answers: {question.answerCounter}</span>
											</p>
										</li>
									))}
								</ul>
							</div>
						);
					}}
				</Query>
			</div>
		);
	}
}

export default withData(UserProfile);