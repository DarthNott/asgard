import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Link from "next/link";
import TimeAgo from "react-timeago";
import styled from "styled-components";
// import checkLoggedIn from "../lib/checkLoggedIn";
import withData from "../lib/withData";
// Queries
import GET_QUESTIONS from "../queries/questions";
// Forms
import CreateQuestion from "../components/forms/createQuestion";
// Title und so in den Head
import Head from 'next/head';

const QuestionList = styled.ul`
	background-color: #f2f2f2;
`

class Questions extends React.Component {
	// static async getInitialProps(context, apolloClient) {
	// 	return { query: context.query.username };
	// }
	constructor(props) {
		super(props);
		this.state = {
			limit: 10
		};
	}

	render() {
		return (
			<div>
				<Head>
					<title>Hello World</title>
				</Head>
				<Query query={GET_QUESTIONS} variables={{ limit: this.state.limit }}>
					{({ loading, error, data }) => {
						if (loading) return "Loading...";
						if (error) return `Error! ${error.message}`;

						return (
							<div>
								<h2>Create Question</h2>
								<CreateQuestion/>
								
								<h2>Questions</h2>
								<QuestionList>
									{data.questions.map((question, index) => (
										<li key={index}>
											<p>
											<Link href={`/question/${question._id}`}><span><b>{question.title}</b> by {question.author.username} ({question.author.xp} XP)</span></Link>
												<br/>
												<span>{question.content}</span>
												<br/>
												<TimeAgo date={question.createdAt} />
												<br/>
												<b>Answers: </b><span>{question.answerCounter}</span>
											</p>
										</li>
									))}
								</QuestionList>
							</div>
						);
					}}
				</Query>
			</div>
		);
	}
}

export default withData(Questions);