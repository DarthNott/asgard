const passport = require("passport");

// Basically for NON-gql routes

module.exports = (server, app) => {
	server.get("/logout", (req, res) => {
		req.logout();
		res.redirect("/");
	});

	// Profilseite nach username
	server.get("/u/:username", (req, res) => {
		const actualPage = "/user";
		const queryParams = { username: req.params.username };
		app.render(req, res, actualPage, queryParams);
	});

	// Question nach questionId
	server.get('/question/:questionId', (req, res) => {
		const actualPage = '/question'
		const queryParams = { questionId: req.params.questionId, answerId: req.params.answerId }
		app.render(req, res, actualPage, queryParams)
	})

	// Zum Scrollen zu einer Antwort einer Frage - klappt noch net
	// server.get('/question/:questionId/answer/:answerId', (req, res) => {
	// 	const actualPage = '/question'
	// 	const queryParams = { questionId: req.params.questionId, answerId: req.params.answerId }
	// 	app.render(req, res, actualPage, queryParams)
	// })
};
