const mongoose = require('mongoose')
const validator = require('validator')
const mongodbErrorHandler = require('mongoose-mongodb-errors')
const shortid = require("shortid")

const Schema = mongoose.Schema
mongoose.Promise = global.Promise

const answerSchema = new Schema({
  _id: {
		type: String,
		default: shortid.generate
	},
  content: {
    type: String,
    required: true
  },
  questionId: {
    type: String,
		// ref: 'Question',
		required: true
  },
  authorId: {
    type: String,//mongoose.Schema.Types.ObjectId,
		// ref: 'User',
		required: true
  },
  likes: {
    type: [String]
  },
  likeCounter: {
    type: Number,
    default: 0,
    min: 0
  },
  createdAt: Date,
	editedAt: Date
})

answerSchema.plugin(mongodbErrorHandler)

module.exports = mongoose.model('Answer', answerSchema)