const mongoose = require('mongoose')
const validator = require('validator')
const mongodbErrorHandler = require('mongoose-mongodb-errors')
const passportLocalMongoose = require('passport-local-mongoose')
const shortid = require('shortid')

const Schema = mongoose.Schema
mongoose.Promise = global.Promise

const userSchema = new Schema({
	_id: {
		type: String,
  	default: shortid.generate
	},
	email: {
		type: String,
		unique: true,
		lowercase: true,
		trim: true,
		validate: {
			isAsync: true,
			validator: (v, cb) =>
				cb(validator.isEmail(v), `${v} is not a valid email address`)
		},
		required: 'Please Supply an email address'
	},
	username: {
		type: String,
		minlength: 3,
		unique: true,
		lowercase: true,
		validate: /^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/,
		required: 'Please Supply a username'
	},
	name: {
		type: String
	},
	avatar: {
		type: Number,
		min: 1,
		max: 5
	},
	questions: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Question'
	},
	answers: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Answer'
	},
	gender: {
		type: String,
		enum: ['Male', 'Female', 'Not Specified'],
		default: 'Not Specified'
	},
	bio: {
		type: String,
		maxlength: 300
	},
	xp: {
		type: Number,
		default: 1,
		min: 1
	},
	createdAt: {
		type: Date,
		default: Date.now()
	},
	nativeLanguage: {
		type: String,
		enum: ['English', 'German', 'Spanish'],
		default: 'German'
	},
	learningLanguage: {
		type: [String],
		enum: ['English', 'German', 'Spanish'],
		default: 'English'
	},
	badges: [{badgeNumber: Number, badgeActive: Boolean}]
})

userSchema.plugin(passportLocalMongoose, {
	usernameField: 'email',
	errorMessages: {
		UserExistsError: 'Email Already Exists'
	}
})

userSchema.plugin(mongodbErrorHandler)

module.exports = mongoose.model('User', userSchema)