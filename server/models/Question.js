const mongoose = require('mongoose')
const validator = require('validator')
const mongodbErrorHandler = require('mongoose-mongodb-errors')
const shortid = require("shortid")

const Schema = mongoose.Schema
mongoose.Promise = global.Promise

const questionSchema = new Schema({
  _id: {
		type: String,
		default: shortid.generate
	},
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  authorId: {
    type: String,
		// ref: 'User',
		required: true
  },
  answers: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Answer'
  },
  answerCounter: {
    type: Number,
    default: 0,
    min: 0
  },
  createdAt: Date,
	editedAt: Date
})

questionSchema.plugin(mongodbErrorHandler)

module.exports = mongoose.model('Question', questionSchema)