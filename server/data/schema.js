const { makeExecutableSchema } = require('graphql-tools')
const resolvers = require('./resolvers')

const typeDefs = `
	type User {
		_id: String
		email: String
		username: String
		avatar: Int
		name: String
		questions: [Question]
		answers: [Answer]
		gender: String
		bio: String
		nativeLanguage: String
		learningLanguage: [String]
		xp: Int
		createdAt: String
		badges: [Badges]
	}

	# to display badges as an array of objects
	type Badges {
		badgeNumber: Int
		badgeActive: Boolean
	}

	type Question {
		_id: String
		title: String
		content: String
		author: User
		authorId: String
		answers: [Answer]
		answerCounter: Int
		createdAt: String
		editedAt: String
	}

	type Answer {
		_id: String
		content: String
		author: User
		authorId: String
		question: Question
		questionId: String
		likes: [String]
		likeCounter: Int
		liked: Boolean
		createdAt: String
		editedAt: String
	}

	type Query {
		profile(
			username: String
		): User

		userList(
			limit: Int!
			skip: Int
		): [User]

		question(
			id: String!
		): Question

		questions(
			limit: Int!
			skip: Int
			author: String
		): [Question]

		answers(
			limit: Int!
			skip: Int
			author: String
			questionId: String
		): [Answer]
	}


	type Mutation {
		createUser(
			email: String!
			username: String!
			password: String!
			avatar: Int
			name: String
			gender: String
			bio: String
			nativeLanguage: String
			learningLanguage: [String]
		): User

		login(email: String!, password: String!): User

		addXP(username: String!, value: Int, badge: Boolean): User

		editProfile(
			email: String
			username: String
			password: String
			avatar: Int
			name: String
			gender: String
			bio: String
			nativeLanguage: String
			learningLanguage: [String]
		): User

		createQuestion(
			title: String!
			content: String!
		): Question

		editQuestion(
			id: String!
			title: String
			content: String
		): Question

		createAnswer(
			questionId: String!
			content: String!
		): Answer

		likeAnswer(
			answerId: String!
		): Answer

		editAnswer(
			id: String!
			content: String
		): Answer
	}
`

module.exports = makeExecutableSchema({ typeDefs, resolvers })
