const User = require('../models/User')
const Question = require('../models/Question')
const Answer = require('../models/Answer')
const passport = require('passport')

module.exports = {
	Query: {
		async profile(root, { username }, req) {
			try {
				if (username) {
					return (user = await User.findOne({
						username: username
					}))
				}
				if (req.user) {
					return req.user
				}
			} catch (err) {
				throw new Error(err.message);
			}
		},
		async userList(root, args) { 
			const page = Math.max(0, args.skip);
			try {
				return (profiles = await User.find({})
					.limit(args.limit)
					.select("-email")
					.skip(args.skip ? args.limit * page : 0)
					.sort({
						createdAt: -1
					}));
			} catch (err) {
				console.error(err);
				return null;
			}
		},
		async question(root, { id }, req) {
			try {
				if (id) {
					return (question = await Question.findOne({
						_id: id
					}))
				}
				if (req.question) {
					return req.question
				}
			} catch (err) {
				throw new Error(err.message);
			}
		},
		async questions(root, args) {
			const page = Math.max(0, args.skip);
			try {
				if (args.author) {
					const user = await User.findOne({
						username: args.author
					})
					return (questions = await Question.find({
							authorId: user._id
						})
						.limit(args.limit)
						.skip(args.skip ? args.limit * page : 0)
						.sort({
							createdAt: -1
						}));
				}
				return (questions = await Question.find({})
					.limit(args.limit)
					.skip(args.skip ? args.limit * page : 0)
					.sort({
						createdAt: -1
					}));
			} catch (err) {
				console.error(err);
				return null;
			}
		},
		// Beispiel: answers(limit: 1, skip: 0) = 1 eintrag pro seite und seite 0 ist zu sehen!
		async answers(root, args) {
			// const page = Math.max(0, args.skip); // RAUS FÜR FETCHMORE
			try {
				if (args.authorId) {
					const user = await User.findOne({
						username: args.author
					})
					return (answers = await Answer.find({
							authorId: user._id
						})
						.limit(args.limit)
						.skip(args.skip) // .skip(args.skip ? args.limit * page : 0)
						.sort({
							createdAt: -1
						}));
				} else if (args.questionId) {
					const question = await Question.findOne({
						_id: args.questionId
					})
					return (answers = await Answer.find({
							questionId: question._id
						})
						.limit(args.limit)
						.skip(args.skip) // .skip(args.skip ? args.limit * page : 0)
						.sort({
							createdAt: -1
						}));
				}
				return (answers = await Answer.find({})
					.limit(args.limit)
					.skip(args.skip ? args.limit * page : 0)
					.sort({
						createdAt: -1
					}));
			} catch (err) {
				console.error(err);
				return null;
			}
		}
	},

	User: {
		questions: root => {
			return Question.find({
				authorId: root._id
			});
		},
		answers: root => {
			return Answer.find({
				authorId: root._id
			})
		}
	},

	Question: {
		author: root => {
			return User.findOne({
				_id: root.authorId
			}).select('-email')
		},
		answers: root => {
			return Answer.find({
				questionId: root._id
			})
		}
	},

	Answer: {
		author: root => {
			return User.findOne({
				_id: root.authorId
			}).select('-email')
		},
		question: root => {
			return Question.findOne({
				_id: root.questionId
			})
		},
		liked: (root, {}, req) => {
			if (req.user) {
				const verify = Answer.count({
					_id: root._id,
					likes: {
						$in: [req.user._id]
					}
				})
				return verify
			}
			return false
		}
	},

	Mutation: {
		createUser(root, { 
			email, 
			username, 
			password, 
			avatar, 
			name, 
			gender, 
			bio, 
			nativeLanguage, 
			learningLanguage 
		}, { login }) {
			const userObject = {
				username: username,
				email: email,
				name: name ? name : username,
				avatar: Math.floor(Math.random() * (5 - 1 + 1)) + 1, // kann User.js -> default: Math...
				gender: gender,
				nativeLanguage: nativeLanguage,
				learningLanguage: learningLanguage,
				bio: bio,
				badges: [
					{ badgeNumber: 1, badgeActive: false },
					{ badgeNumber: 2, badgeActive: false },
					{ badgeNumber: 3, badgeActive: false }
				]
			}
			const user = new User(userObject) // const user = new User({ email, username })

			return new Promise((resolve, reject) => {
				return User.register(user, password, err => {
					if (err) {
						reject(err)
					} else {
						login(user, () => resolve(user))
					}
				})
			})
		},
		login(root, { email, password }, { login }) {
			return new Promise((resolve, reject) => {
				return User.authenticate()(email, password, (err, user) => {
					// user returns false if username / email incorrect
					if (user) {
						login(user, () => resolve(user))
					} else {
						reject('Email / Password Incorrect')
					}
				})
			})
		},
		// MUTATION - Increase XP - KANN WEG UND DAFÜR IN QUESTION, ANSWER, LIKE rein
		async addXP(root, { username, value }, req) {
			try {
				const user = await User.findOne({
					username: username // sucht nach username (benötigt keinen login)
				});

				user.set({
					xp: user.xp + (value === undefined ? 1 : value)
				})

				let badgeObject = user.badges;

				if(user.xp >= 5) {
					badgeObject[0].badgeActive = true
				}

				if(user.xp >= 10) {
					badgeObject[1].badgeActive = true
				}

				if(user.xp >= 15) {
					badgeObject[2].badgeActive = true
				}

				user.set({
					badges: badgeObject
				})

				const updatedUser = await user.save();
				return updatedUser;
			} catch (err) {
				throw new Error(err.message);
			}
		},
		// MUTATION - Edit Profile - args anstatt username... -> username: args.username ? args.username : req.user.username
		async editProfile(root, { 
			username, 
			email, 
			avatar, 
			name, 
			gender, 
			nativeLanguage, 
			learningLanguage, 
			bio 
		}, req, { profile }) {
			try {
				const user = await User.findOne({
					_id: req.user._id
				});

				if (user) {
					user.set({
						username: username ? username : req.user.username,
						avatar: avatar ? avatar : req.user.avatar,
						nativeLanguage: nativeLanguage ? nativeLanguage : req.user.nativeLanguage,
						email: email ? email : req.user.email,
						bio: bio ? bio : req.user.bio,
						name: name ? name : req.user.name,
						gender: gender ? gender : req.user.gender,
						learningLanguage: learningLanguage ? learningLanguage : req.user.learningLanguage
					});
					const updatedUser = await user.save();
					return updatedUser;
				}
			} catch (err) {
				throw new Error(err.message);
			}
		},

		async createQuestion(root, { title, content }, req) {
			try {
				if (req.user) {
					let question = new Question({
						authorId: req.user._id,
						title: title,
						content: content,
						createdAt: Date.now(),
						editedAt: Date.now()
					})
					const createdQuestion = await question.save();

					// SET XP + 3
					req.user.set({
						xp: req.user.xp + 3
					})
					await req.user.save()

					return createdQuestion
				}
			} catch (err) {
				throw new Error(err.messag)
			}
		},
		async editQuestion(root, { id, title, content }, req)  {
			try {
				const question = await Question.findOne({
					_id: id
				})

				// Error wenn man nicht eingeloggt ist
				if (!req.user) {
					throw new Error('You have to be logged in to edit your own question.')
				}

				// Man kann nur seine Frage bearbeiten
				if (req.user._id.toString() == question.authorId.toString()) {
					if (question) {
						question.set({
							title: title ? title : question.title,
							content: content ? content : question.content,
							editedAt: Date.now()
						})
						const updatedQuestion = await question.save()
						return updatedQuestion
					}
				}
				
			} catch (err) {
				throw new Error(err.message)
			}
		},

		async createAnswer(root, { questionId, content }, req) {
			try {
				const question = await Question.findOne({
					_id: questionId
				})
				// Error wenn man nicht eingeloggt ist
				if (!req.user) {
					throw new Error('You have to be logged in.')
				}

				if (req.user) { // Man muss angemeldet sein
					if (questionId) {	// Es muss eine Frage zum Beantworten ausgewählt sein
						let answer = new Answer({
							authorId: req.user._id,
							questionId: question._id,
							content: content,
							createdAt: Date.now(),
							editedAt: Date.now()
						})
						const createdAnswer = await answer.save()
						
						const answerCounter = await Answer.count({
							questionId: questionId
						})

						// Zum Zählen von Antworten nutzen!!!
						question.set({
							answerCounter: answerCounter
						})
						await question.save()
						
						// SET XP + 1
						req.user.set({
							xp: req.user.xp + 1
						})
						await req.user.save()

						return createdAnswer
					}
				}
			} catch (err) {
				throw new Error(err.message)
			}
		},
		async likeAnswer(root, { answerId }, req) {
			// Eingeloggt?
			if (!req.user) {
				throw new Error('Not logged in')
			}
			// Datenbank durchsuchen
			const user = await User.findOne({
				_id: req.user._id
			})
			const answer = await Answer.findOne({
				_id: answerId
			})

			// Selbe Benutzer
			if (req.user._id.toString() == answer.authorId.toString()) {
				throw new Error('Same user')
			}
			// Check ob abgestimmt hat
			const verify = await Answer.count({
				_id: answer._id,
				likes: {
					$in: [req.user._id]
				}
			})

			try {
				if (verify) {
					// Remove like
					answer.likes.pull(req.user._id)
					answer.likeCounter--
					const likedAnswer = await answer.save()
					return likedAnswer
				} else {
					// Add like
					answer.likes.push(req.user._id)
					answer.likeCounter++
					const likedAnswer = await answer.save()
					return likedAnswer
				}
			} catch (err) {
				throw new Error(err.message)
			}
		},
		async editAnswer(root, { id, content }, req) {
			try {
				const answer = await Answer.findOne({
					_id: id
				})
				// Error wenn man nicht eingeloggt ist
				if (!req.user) {
					throw new Error('You have to be logged in to edit your own answer.')
				}

				// Man kann nur seine Antwort bearbeiten
				if (req.user._id.toString() == answer.authorId.toString()) {
					if (answer) {
						answer.set({
							content: content ? content : answer.content,
							editedAt: Date.now()
						})
						const updatedAnswer = await answer.save()
						return updatedAnswer
					}
				}
			} catch (err) {
				throw new Error(err.message)
			}
		}

	}
}

/*
createQuestion(root, { title, content }, req) {
			return new Promise((resolve, reject) => {
				if (req.user) {
					let question = new Question({
						authorId: req.user._id,
						title: title,
						content: content,
						createdAt: Date.now(),
						editedAt: Date.now()
					})
					question.save();
					resolve(question);
				} else {
					reject("Not logged in");
				}
			})
		},
*/

/* 
async questions(root, args) {
			try {
				if (args.authorId) {
					return (questions = await Question.find({
							authorId: args.authorId
						})
						.limit(args.limit)
						.skip(args.skip)
						.sort({
							createdAt: -1
						}));
				}
				return (questions = await Question.find({})
					.limit(args.limit)
					.skip(args.skip)
					.sort({
						createdAt: -1
					}));
			} catch (err) {
				console.error(err);
				return null;
			}
		},
*/

/*
async addXP(root, { username, value, badge }, req) {
			try {
				// Durchsuche die Datenbank nach dem User wenn er eingeloggt ist
				// const user = await User.findOne({
				// 	_id: req.user._id
				// })
				// const user = await User.findOne();
				const user = req.user;
				const user = req.user ? "datenbankAbfrage mit den usernamen von args" : req.user;
				// ? addXp(username: "nott") soll nott xp geben, egal ob eingeloggt oder net

				// Erhöhe XP
				if (user) {
					user.set({
						xp: user.xp + (value === undefined ? 1 : value)
					})

					if (badge) {
						user.badges.pop();
					}

					// eine weitere "unnötige Abfrage" -- Malte
					const verify = await User.count({ // .count({}) zählt die Dokumente // .find({}) gibt dafür das gefundene Dokument aus 👍🏻
						// _id: req.user._id,
						badges: {
							$in: ['5']
						}
					}); // gibt 0 wenn nix gefunden wird und 1 wenn er es findet

					let badgesArray = ['5', '10']
					// let index = user.badges.findIndex(badge => badge === '5');
					// console.log(index)
					// if (user.xp >= badgesArray[0] && user.badges[index] !== badgesArray[0]) {
					// 	console.log('not there')
					// 	user.badges.push('5');
					// }

					// for (let badgeIndex = 0; badgeIndex < badgesArray.length; badgeIndex++) {
					// 	let index = user.badges.findIndex(badge => badge == badgesArray[badgeIndex]);
					// 	console.log(index)
					// 	if (user.xp >= badgesArray[badgeIndex] && user.badges[index] !== badgesArray[badgeIndex]) {
					// 		console.log('true')
					// 		user.badges.push('5'); // das haut noch nicht so ganz hin
					// 	}
					// }
					

					// Nicht vergessen!!! for loop switch und so für badge, DRY!
					// Vllt mit JSON, wo die Werte und co gespeichert sind? -- Malte
					{
						xp: {
							10: "badge Id 20",
							20: "badge Id 545"
						},
						comments: {
							1: {
								badgeAwardID: 5,
								message: "blaaa blaa glückwunsch blaa" 👍🏻
							}
						},
						posts: {
							5: "badge Id 2"
						}
					}
					// Würde es in eine andere DAtei machen, sonst wird die hier zu groß -- Malte

					// let index = user.badges.findIndex(badge => badge === '5'); if(user.badges[index] !== '5')
					 
					// WIEDER AKTIVIEREN
					if (user.xp >= 5 && !verify) { 
						user.badges.push('5');
					}
					const updatedUser = await user.save();
					return updatedUser;
				}
			} catch (err) {
				throw new Error(err.message);
			}
		},
*/